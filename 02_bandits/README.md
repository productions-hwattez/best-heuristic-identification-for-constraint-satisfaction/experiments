# Organisation of the campaign

## Goal of the campaign

This campaign aims to include the state-of-the-art heuristics from the previous campaign, but also to experiment with the state-of-the-art bandits as well as the new ASH bandit (without sampling).

## Description of the hierarchy

- `config`
	- metrics_scalpel.yml` : campaign description file (solver logs, cpu time, memory...)
	- `xp.properties` : description specific to the calculation cluster used
- `experiment_wares` : file describing the campaign
	- `./solver1` : executable of a solver to run on all instances (contained in `input_set`)
	- `./solver2` : executable of a solver to run on all instances (contained in `input_set`)
	- `./solver3` : executable of a solver to be executed on all instances (contained in `input_set`)
	- `...` : ...as many as necessary to complete the campaign
- `input_set`
	- ...`: list of instances or a file listing the location of instances
- `experiments` : corresponds to the set of logs classified hierarchically by solvers, then by instances
    - solver1
        - instance1
            - `execution.out` : the standard output of `solver1` for the `instance1` instance
            - `statistics.out` : the final time and memory statistics built by `runsolver`.
            - `stdout`: the standard output of `runsolver` is redirected to this file
            - `stderr`: error output from the solver and `runsolver` are redirected to this file
        - `instance2`
            - `...`
        - `instance3`
            - `...`
        - `...`
    - solver2` `...`.
        - `...`
    - solver3` `...`.
        - `...`
- `fig`
	- `[1-9][A-Z]-.*[svg|png|pdf|tex]` : 
        - the set of figures produced by the notebooks
        - each figure is indicated by the following pattern `[1-9][A-Z]-.*` linking the figure to a specific notebook (notebook descriptions are given below)
- `tools`
	- `*.sh` : 
        - corresponds to the set of scripts necessary to send the present campaign on the CRIL-LENS cluster-B
        - these scripts need to be adapted to work in another computing environment
- export' : extraction and export of logs in this folder
    - `[1-9]-*.[bin|csv]` :
        - binary logs are large and are not kept in the git directory
        - csv logs are reduced in size and kept in the git directory
        - logs are indexed by a number between 1 and 9 corresponding to the notebook that produced them
- 1-9][A-Z]?.*ipynb` :
    - the titles of the notebooks containing the extracted solver logs start with a number (and no letter) and are ordered by it. The order must be respected because previous logs may be needed.
    - The titles of the notebooks containing the analysis of the campaigns start with a number (corresponding to a specific extraction), then a letter. The order is not important. Each notebook produces one or more figures.
    
    
## Additional information

- When the result of the exports is not too large, they are committed to the directory (in csv). It is therefore possible to analyse these data directly, otherwise it is necessary to run the campaign again.
- The tools available in the *tools* directory are adapted for the CRIL cluster, as well as the logs layout in the *experiments* folder (the latter is not visible in the directory because it is too bulky). The user wishing to run a campaign again will have to adapt the tools and possibly the logs layout and their reading thanks to the *Scalpel* tool and its configuration file `config/metrics_scalpel.yml`.