#!/bin/bash

REPO='https://gitlab.univ-artois.fr/productions/single-tournament/ace'
DIR='ace'

cd $(dirname $0)
if [ ! -d $DIR ]
then
    git clone $REPO
fi
cd $DIR
git pull
./gradlew build -x test