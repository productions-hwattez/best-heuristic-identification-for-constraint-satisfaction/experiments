# Experiments of the article: Best Heuristic Identification for Constraint Satisfaction

- Solver used: [ACE](/../../../../ace)
- Instances used: [XCSP instances](/../../../../xcsp-instances) (only CSP)

## The campaigns

The experiments are divided into three campaigns:

- the first one concerns the state of the art base composed of solvers with simple heuristics
- the second includes the use of the state-of-the-art bandits and the ASH bandit (without sampling)
- the last one (where all the figures of the article are produced) is composed of the results of the previous campaigns + ASH with sampling
