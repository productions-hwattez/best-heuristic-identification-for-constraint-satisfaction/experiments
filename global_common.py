import matplotlib as mpl
c = [x['color'] for x in mpl.rcParams["axes.prop_cycle"]]
from autograph.core.enumstyle import LineType
import os
FILE = __file__[:__file__.rfind('/')+1]

# LOGS EXPORTÉS

CAMPAIGN_01_BIN = FILE + '01_base/export/01-campaign.bin'
CAMPAIGN_01_CSV = FILE + '01_base/export/01-campaign.csv'

CAMPAIGN_02_BIN = FILE + '02_bandits/export/01-campaign.bin'
CAMPAIGN_02_CSV = FILE + '02_bandits/export/01-campaign.csv'
CAMPAIGN_02_MERGE_CSV = FILE + '02_bandits/export/02-merge.csv'

CAMPAIGN_03_BIN = FILE + '03_bandits_samp/export/01-campaign.bin'
CAMPAIGN_03_CSV = FILE + '03_bandits_samp/export/01-campaign.csv'
CAMPAIGN_03_MERGE_CSV = FILE + '03_bandits_samp/export/02-merge.csv'
CAMPAIGN_03_MERGE_BIN = FILE + '03_bandits_samp/export/04-campaign.bin'

# AUTRES CONSTANTES

SCALPEL_INPUT_FILE = 'config/metrics_scalpel.yml'

ORDER = [
    'activity',
    'impact',
    'domddeg',
    'chs',
    'cacd',
    'VBS'
]

ORDER_B = [
    'lex',
    'deg',
    'rand',
    'VBS'
]

MAP_NAME = {
    'Wdeg-CACD': '$\mathtt{cacd}$',
    'WdegOnDom-CHS': '$\mathtt{chs}$',
    'Lexico': '$\mathtt{lex}$',
    'WdegOnDom-UNIT': '$\mathtt{dom/wdeg}$',
    'Activity': '$\mathtt{abs}$',
    'DdegOnDom': '$\mathtt{dom/ddeg}$',
    'Dom': '$\mathtt{dom}$',
    'Impact': '$\mathtt{ibs}$',
    
    'cacd': '$\mathtt{cacd}$',
    'chs': '$\mathtt{chs}$',
    'lex': '$\mathtt{lex}$',
    'wdeg': '$\mathtt{dom/wdeg}$',
    'activity': '$\mathtt{abs}$',
    'uni': '$\mathtt{UNI}$',
    'domddeg': '$\mathtt{dom/ddeg}$',
    'dom': '$\mathtt{dom}$',
    'impact': '$\mathtt{ibs}$',

    'VBS': '$\mathtt{VBS}$',

    'esb_ucb': '$\mathtt{UCB}$',
    'auvr_exp3': '$\mathtt{EXP3}$',
    'npts_exp3': '$\mathtt{EXP3}$',
    'esb_exp3': '$\mathtt{EXP3}$',
    'st_multivarh_npts': '$\mathtt{AST}_{1}$',
    'st_multivarh_esb': '$\mathtt{AST}_{1}$',
    'auvr_ucb': '$\mathtt{UCB}$',
    'npts_ucb': '$\mathtt{UCB}$',
    'st_multivarh_auvr': '$\mathtt{AST}_{1}$',
    'st_multivarh_esb_M_8': '$\mathtt{AST}_{8}$',
    'st_multivarh_npts_M_16': '$\mathtt{AST}_{16}$',
    'st_multivarh_npts_M_8': '$\mathtt{AST}_{8}$',
    'st_multivarh_auvr_M_8': '$\mathtt{AST}_{8}$',
    'st_multivarh_auvr_M_16': '$\mathtt{AST}_{16}$',
    'st_multivarh_esb_M_4': '$\mathtt{AST}_{4}$',
    'st_multivarh_esb_M_2': '$\mathtt{AST}_{2}$',
    'st_multivarh_auvr_M_2': '$\mathtt{AST}_{2}$',
    'st_multivarh_npts_M_2': '$\mathtt{AST}_{2}$',
    'st_multivarh_npts_M_4': '$\mathtt{AST}_{4}$',
    'st_multivarh_auvr_M_4': '$\mathtt{AST}_{4}$',
    'st_multivarh_esb_M_16': '$\mathtt{AST}_{16}$'
}

MAP_NAME_R = {v: k for k, v in MAP_NAME.items()}